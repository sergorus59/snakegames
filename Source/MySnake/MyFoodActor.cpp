// Fill out your copyright notice in the Description page of Project Settings.


#include "MyFoodActor.h"
#include "MySnake.h"

#include "MySnakeActor.h"

// Sets default values
AMyFoodActor::AMyFoodActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//������� �������� ������� ��� ������
	MyRootComponent = CreateDefaultSubobject<USphereComponent>("RootEeat");
	//��������� ���� � ������
	RootComponent = MyRootComponent;

	//����� ����������� ������ UE � ����� �� �� ������ ������� ���� ������
	SnakeEeatMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Sphere")).Object;

	//�������� ��� ���
	class UMaterialInstance* EeatColor;
	//��������� �������� ��� ���
	EeatColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/SnakeContent/Material/InstanceMaterial/M_Metal_Gold_Inst.M_Metal_Gold_Inst'")).Get();

	//������ ���� ���
	FVector Size = FVector(0.5f, 0.5f, 0.5f);

	//������� ����������� ���� ��� �������� ����� � ������
	class UStaticMeshComponent* EEatChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Eeat"));

	//������ ����������� ����� ��� �����
	EEatChank->SetStaticMesh(SnakeEeatMesh);
	//������� ����� ������� ��� ������
	EEatChank->SetRelativeScale3D(Size);
	//������� ��� ����� ��� �������
	EEatChank->SetRelativeLocation(FVector(0, 0, 0));
	//������� ��� ����� ��� ��������
	EEatChank->SetMaterial(0, EeatColor);
	//������������ ����
	EEatChank->AttachTo(MyRootComponent);

	//������� ��� ����� ��� ��������� ������
	EEatChank->SetSimulatePhysics(true);
}

// Called when the game starts or when spawned
void AMyFoodActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyFoodActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//������������ ����������� ����������� �������
	BufferTime += DeltaTime;

	if (BufferTime > StepDelay)
	{
		Destroy(true, true);
		//BufferTime = 0;
	}

	CollectEat();

}

// ������ ����� ���
void AMyFoodActor::CollectEat()
{
	//������� ������ ���� ����� �������� ���� � ��� �����������
	TArray<AActor*> CollectedActors;
	//��������� ���� ������� � �������� �����������
	GetOverlappingActors(CollectedActors);

	//��������� ���� � ��� ���� ������������
	for (int32 i = 0; i < CollectedActors.Num(); ++i)
	{
		AMySnakeActor* const Test = Cast<AMySnakeActor>(CollectedActors[i]);

		if (Test)
		{
			Test->VisibleBodyChank++;
			
			Test->Score++;

			//���������� ������
			Destroy(true, true);
			//��� ��� ������ �� ����� �� ��� ������ ������ ���������
			break;//��������� ����
		}
	}
}

