// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SceneComponent.h"
#include "Components/SphereComponent.h"
#include "UObject/Object.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "MyFoodActor.generated.h"

UCLASS()
class MYSNAKE_API AMyFoodActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyFoodActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//������� ����� ������� ����� ������ ���� �������� ��� ����� ���
	class USphereComponent* MyRootComponent;

	//������� �������� ������� ����� ������ ���� ��� � ����
	class UStaticMesh* SnakeEeatMesh;

	//������ ����� ���
	void CollectEat();

	//�������� �� ������
	float StepDelay = 7.f;
	//���������� �������
	float BufferTime = 0;

};
