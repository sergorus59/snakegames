// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UObject/Object.h"
#include "Components/BoxComponent.h"
#include "Components/SceneComponent.h"
#include "MyDethActor.generated.h"

UCLASS()
class MYSNAKE_API AMyDethActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyDethActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//������� ���� ������� ����� ������ ���� �����
	UPROPERTY(EditAnywhere)
		class UBoxComponent* MyRootComponent;
	//�������� ��� �����
	UPROPERTY(EditAnywhere)
		class UMaterialInstance* WallCollor;

	void CollideWall();

};
