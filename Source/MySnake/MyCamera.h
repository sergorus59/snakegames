// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/BoxComponent.h"
#include "MyCamera.generated.h"

UCLASS()
class MYSNAKE_API AMyCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyCamera();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//�������� ���� ��� ������ ������
	UPROPERTY(EditAnywhere)
		class UBoxComponent* MyRootComponent;

	//������ ��� ������
	UPROPERTY(EditAnywhere)
		class USpringArmComponent* CameraSpring;

	//������ ���������
	UPROPERTY(EditAnywhere)
		class UCameraComponent* MyCamera;

	//���� ����� ������� ����� ������ �� ������� ����
	class AMySnakeActor* MySnakePlayer;

	//������ ������� ������� ����� ������ ������ ����� �� ���������������
	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
		//������ ������� ������� ����� ��������� ������ �� ������
		void AddSnakeToMap();

	void FMove(float ButtonVal);

	//����������� �������� ������
	FVector2D WASD;

	//�������� �� Y ���� �������� �����
	float MinY = -1500;	float MaxY = 1500;
	float MinX = -800; 	float MaxX = 800;
	//����� �� ��� Z �������� �����
	float SpawnZ = 50.f;

	//������� ������� �������� ��� � ��������� ������ �����
	void AddRandomFood();

	//�������� ��� ��������
	float StepDelay = 0.5f;
	//���������� �������
	float BufferTime = 0;

	//���������� ������ ����
	int32 GameMode = 0;

	//������ ������� ������� ����� ������ ������ ����� �� ���������������
	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
		int32 GetGameMode() const { return GameMode; }

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
		int32 GetScore();

	//��������� ���������� �����
	bool GamePause = false;

	//������� ����� ���������� ������ ����� � Blueprint �����
	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
		bool GetGamePause() const {return GamePause;}

	//������� ��������� ������
	void SnakeDestroy();
};
