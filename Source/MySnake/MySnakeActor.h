// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "UObject/Object.h"

#include "MySnakeActor.generated.h"

UCLASS()
class MYSNAKE_API AMySnakeActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMySnakeActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//������ ������������ ������ ������ ��� �� ��������
	UPROPERTY(EditAnywhere) int32 SnakeSize = 200;
	//������ ���� ������ ����� ������ ����� ����� ������
	float StepSnake = 45.f;
	//������ ������� ����� ������� ����� ���� ������
	TArray<UStaticMeshComponent*> SnakeBody;

	//�������� ���� ������
	class USphereComponent* MyRootComponent;

	//������� �������� ���� ������
	void CreateSnakeBody();

	//������ ����������� ������� ������ ������
	UPROPERTY(EditAnywhere)
		int32 VisibleBodyChank = 3;

	//������� ���������� ������� ������ ������ �����
	void SetVisibleChank();


	//���������� � ����� ������� ��������� ������ � ������� ������
	UPROPERTY(EditAnywhere)
		FVector2D DirectionMoveSnake;

	//�������� ��� ��������
	float StepDelay = 0.3f;
	//���������� �������
	float BufferTime = 0;

	//������� �������� ������ �� ��������� �����������
	void MoveSnake();

	int32 Score = 0;

	//��� �������� ������ ����
	class AMyCamera* WhoPawn;

	//������� ��������� ����� �������
	void HaveDamage();

	//������� ���������� �������� ������
	//float SpeedSnakeUp();
};
