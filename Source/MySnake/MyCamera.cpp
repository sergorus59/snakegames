// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCamera.h"
#include "MySnake.h"
//�������� ��� ����� ����� ������ ������
#include "MySnakeActor.h"
//��������� ������ �� ����� �����
#include "MyFoodActor.h"

// Sets default values
AMyCamera::AMyCamera()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    //������� �������� ������� ��� ������
    MyRootComponent = CreateDefaultSubobject<UBoxComponent>("RootModel");  
    //��������� ���� � �����
    RootComponent = MyRootComponent;

    //�������� ������ ��� ������
    CameraSpring = CreateDefaultSubobject<USpringArmComponent>("Spring");
    //������������� ������� �������
    CameraSpring->SetRelativeLocation(FVector(0, 0, 0));
    //������������ ��� � ������
    CameraSpring->AttachTo(MyRootComponent);

    //������� ������
    MyCamera = CreateDefaultSubobject<UCameraComponent>("Camera");
    //����������� ������ � �������
    MyCamera->AttachTo(CameraSpring, USpringArmComponent::SocketName);

    //������������� ������� �������
    CameraSpring->SetRelativeRotation(FRotator(-90.f, 0, 0));
    //������������� ������ �������
    CameraSpring->TargetArmLength = 1700.f;
    //��������� �������� ������������ � ����� �������
    CameraSpring->bDoCollisionTest = false;

    //������������� ���������� �� ������ 0
    AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void AMyCamera::BeginPlay()
{
    Super::BeginPlay();
   
    
}

// Called every frame
void AMyCamera::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (GameMode > 0)
    {
        //���������� ����������� ���������� ��������
        BufferTime += DeltaTime;
        if (BufferTime > StepDelay)
        {
            if (!GamePause) AddRandomFood();
            BufferTime = 0;
        }
    }
}

// Called to bind functionality to input
void AMyCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    //�������� ��������� �� ������ W S A D
    InputComponent->BindAxis("KeyMapMove", this, &AMyCamera::FMove);

}

void AMyCamera::AddSnakeToMap()
{
    //�������� ������� ��� ����������� ��������� ����� ���� (Root Model)
    FVector StartPoint = GetActorLocation();
    //�������� ������� ����� ������
    FRotator StartPointRotator = GetActorRotation();

    //���� ����� ���������� ��������� ��������� ������ ������ � ��������� �����
    if (GetWorld())
    {
        MySnakePlayer = GetWorld()->SpawnActor<AMySnakeActor>(StartPoint, StartPointRotator);
        GameMode = 1;
        MySnakePlayer->WhoPawn = this;
    }
}

//����������� �������� ������
void AMyCamera::FMove(float ButtonVal)
{
    //�������� ����� ����� �� ������
    int32 Key = ButtonVal;

    if (GameMode > 0)
    {
        if (Key == 5)
        {
            //����������� ����� ����� � ����
            GamePause = !GamePause;
        }

        if (!GamePause)
        {
            //��������� ��� ����� �����
            switch (Key)
            {
                //W
            case 1:
                if (WASD.X != 3)
                {
                    WASD = FVector2D(0, 0);
                    WASD.X = -3;
                }
                break;

                //S
            case 2:
                if (WASD.X != -3)
                {
                    WASD = FVector2D(0, 0);
                    WASD.X = 3;
                }
                break;

                //D
            case 3:
                if (WASD.Y != 3)
                {
                    WASD = FVector2D(0, 0);
                    WASD.Y = -3;
                }
                break;

                //A
            case 4:
                if (WASD.Y != -3)
                {
                    WASD = FVector2D(0, 0);
                    WASD.Y = 3;
                }
                break;
            }

            if (MySnakePlayer)
            {
                MySnakePlayer->DirectionMoveSnake = WASD;
            }
        }

        else
        {
            MySnakePlayer->DirectionMoveSnake = FVector2D(0, 0);
        }
    }
}

//������� ������� �������� ��� � ��������� ������ �����
void AMyCamera::AddRandomFood()
{
    //������ ������� ��� ������
    FRotator StartPointRotation = FRotator(0, 0, 0);

    //�������� ��������� ����� �� � ��� �������� ������
    float SpawnX = FMath::FRandRange(MinX, MaxX);

    // �������� ��������� ����� �� Y ��� �������� ������
    float SpawnY = FMath::FRandRange(MinY, MaxY);

    //������� ����� ��� ���������� ����� � ���� ����
    FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

    //���� ������ ���������� �� ����� ��������� ������
    if (MySnakePlayer)
    {
        //���� ����� ���� ���������� �� ������� ������
        if (GetWorld())
        {
            GetWorld()->SpawnActor<AMyFoodActor>(StartPoint, StartPointRotation);
        }
    }
}

int32 AMyCamera::GetScore()
{
    if (MySnakePlayer)
    {
        return MySnakePlayer->Score;     
    }
    return 0;
}

void AMyCamera::SnakeDestroy()
{
    //��������� ���� � ����� ���������� ����
    GameMode = 0;
    //���������� ������ ���� ��� ����
    if (MySnakePlayer)
    {
        MySnakePlayer->Destroy(true, true);
    }
}

